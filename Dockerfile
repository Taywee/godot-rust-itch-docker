FROM ubuntu:latest

MAINTAINER Taylor C. Richberger <taywee@gmx.com>

ARG GODOT_VERSION=3.2.3
ARG GODOT_RELEASE=stable

RUN apt update
RUN apt upgrade -y
RUN apt install -y mingw-w64 build-essential unzip wget clang zip
RUN wget -O rustup.sh https://sh.rustup.rs
RUN sh rustup.sh -y
RUN ~/.cargo/bin/rustup target add x86_64-pc-windows-gnu
RUN wget https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-${GODOT_RELEASE}_linux_headless.64.zip
RUN wget https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-${GODOT_RELEASE}_export_templates.tpz
RUN unzip Godot_v${GODOT_VERSION}-${GODOT_RELEASE}_linux_headless.64.zip
RUN mv Godot_v${GODOT_VERSION}-${GODOT_RELEASE}_linux_headless.64 /usr/bin/godot
RUN mkdir -p ~/.local/share/godot/templates
RUN unzip Godot_v${GODOT_VERSION}-${GODOT_RELEASE}_export_templates.tpz
RUN mv templates ~/.local/share/godot/templates/${GODOT_VERSION}.${GODOT_RELEASE}
RUN mkdir -p ~/.cache 
RUN mkdir -p ~/.config/godot
RUN wget -O butler.zip https://broth.itch.ovh/butler/linux-amd64/LATEST/archive/default
RUN unzip butler.zip
RUN chmod +x butler
RUN mv butler /usr/bin/butler
RUN rm -f *.zip *.tpz rustup.sh
